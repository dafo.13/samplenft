from eth_account.messages import encode_defunct
from web3.auto import w3
from web3 import Web3
private_key="privatekey" # privatekey - replace with whiteLister privatekey - 64 character long string 
result = Web3.soliditySha3(['address'], ["eth_address"]) # eth_address - replace with address of the user getting whitelisted - 42 character long string
message = encode_defunct(hexstr=result.hex())
signed_message = w3.eth.account.sign_message(message, private_key=private_key)

print(signed_message)
# example output: SignedMessage(messageHash=HexBytes('0x111ae23894d519b3d919ecb340812b3b9fae4721820f453e4dcc75215a2cd532'), r=109336861364232220166288162868228602259647957364523691762174587221686530321249, s=14069007817266968527496210507949177646687472837625719509623063113740857345149, v=28, signature=HexBytes('0xf1ba7872b0d06de4563aa476761895d14654266614f86867f28094543743ab611f1ac6b74c7b91e4f53efae08ffee3db6378998861a4cc86a2189f99bfbb847d1c'))
# signature hash used in mintWhiteListed function -> 0xf1ba7872b0d06de4563aa476761895d14654266614f86867f28094543743ab611f1ac6b74c7b91e4f53efae08ffee3db6378998861a4cc86a2189f99bfbb847d1c