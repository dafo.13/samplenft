// SPDX-License-Identifier: MIT
pragma solidity ^0.8.9;

import "@openzeppelin/contracts/utils/Counters.sol";
import "@openzeppelin/contracts/token/ERC721/extensions/ERC721URIStorage.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/utils/cryptography/ECDSA.sol";
import "@openzeppelin/contracts/utils/cryptography/SignatureChecker.sol";
import "@openzeppelin/contracts/finance/PaymentSplitter.sol"; 

interface ExternalContract {
    function balanceOf(address owner) external view returns (uint256 balance); // external contract interaction - balanceOf will relay the amount of tokens an address holds
}

contract SampleNFT is ERC721URIStorage, Ownable, PaymentSplitter {

    using Counters for Counters.Counter; 
    Counters.Counter private _tokenIds;
    
    string baseURI;

    uint public MAX_TOKENS = 25000; // max tokens
    uint public MIN_PRICE = 1000000000000000; // 0.001 Eth
    uint8[2][] public PRICING=[[1,100],[5,85],[10,75]]; // [max_bought_tokens,price_multiplyer] both in bounds of uint8 - max 255
    
    
    bool public hasSaleStarted = false;
    address public whiteLister; // address of whitelister - default is 0x000...
    ExternalContract public ownNFTwhitelist; // Placeholder for contract address of past tokens
    
    constructor(string memory name, string memory symbol, string memory base_URI, address[] memory payees, uint256[] memory shares) ERC721(name, symbol) PaymentSplitter(payees, shares) {
       baseURI=base_URI;
    }
    
    function setBaseURI(string memory baseURI_) external {
        baseURI = baseURI_;
    }
    
    function _baseURI() internal view override returns (string memory) {
        return baseURI;
    }
    
    function startSale() public onlyOwner {
        hasSaleStarted = true;
    }
    
    function pauseSale() public onlyOwner {
        hasSaleStarted = false;
    }
    
    function setWhitelister(address _whiteList) public onlyOwner {
        whiteLister = _whiteList;
    }

    function setOwnNFTwhitelist(address oldContractAddress) public onlyOwner {
        ownNFTwhitelist = ExternalContract(oldContractAddress);
    }
    
    function setupPricing(uint8[2][] memory pricing) public onlyOwner  {
        PRICING = pricing;
    }

    function calculatePrice(uint8 numTokens) public view returns (uint) {
        uint price = MIN_PRICE;
        for (uint8 i=0; i<PRICING.length; i++) {
            if (numTokens >= PRICING[i][0]) {
                price = MIN_PRICE * PRICING[i][1];
            }
            else break;
        }
        return price;
    }

    function mint(uint8 numTokens) public payable {
        require(hasSaleStarted == true, "Sale hasn't started");
        require(numTokens > 0 && numTokens <= 10, "Machine can dispense a minimum of 1, maximum of 10 tokens");//needs to be updated
        require(_tokenIds.current()+ numTokens <= MAX_TOKENS , "Exceeds maximum token supply.");
        require(msg.value >= (calculatePrice(numTokens) * numTokens), "Amount of Ether sent is not correct.");
        
        for (uint i = 0; i < numTokens; i++) {
            _tokenIds.increment();
            _mint(msg.sender, _tokenIds.current());
            _setTokenURI(_tokenIds.current(), Strings.toString(_tokenIds.current()));
        }
    }
    
    function mintWhiteListed(uint8 numTokens, bytes memory signature) public payable {

        bytes32 messageHash = keccak256(abi.encodePacked(msg.sender));
        bytes32 ethMessageHash = ECDSA.toEthSignedMessageHash(messageHash);

        require(SignatureChecker.isValidSignatureNow(whiteLister, ethMessageHash, signature), "Address not whitelisted.");
        require(numTokens > 0 && numTokens <= 10, "Machine can dispense a minimum of 1, maximum of 10 tokens");//needs to be updated
        require(_tokenIds.current()+ numTokens <= MAX_TOKENS , "Exceeds maximum token supply.");
        require(msg.value >= (calculatePrice(numTokens) * numTokens), "Amount of Ether sent is not correct.");
        
        for (uint i = 0; i < numTokens; i++) {
            _tokenIds.increment();
            _mint(msg.sender, _tokenIds.current());
            _setTokenURI(_tokenIds.current(), Strings.toString(_tokenIds.current()));
        }
        
    }

    function mintOwnNFT(uint8 numTokens) public payable {
        require(ownNFTwhitelist.balanceOf(msg.sender) > 0, "You don't own any ABS NFTs");
        require(numTokens > 0 && numTokens <= 10, "Machine can dispense a minimum of 1, maximum of 10 tokens");//needs to be updated
        require(_tokenIds.current()+ numTokens <= MAX_TOKENS , "Exceeds maximum token supply.");
        require(msg.value >= (calculatePrice(numTokens) * numTokens), "Amount of Ether sent is not correct.");
        
        for (uint i = 0; i < numTokens; i++) {
            _tokenIds.increment();
            _mint(msg.sender, _tokenIds.current());
            _setTokenURI(_tokenIds.current(), Strings.toString(_tokenIds.current()));
        }
    }
    
    function magicMint(uint8 numTokens) external onlyOwner {
        require(_tokenIds.current()+ numTokens <= MAX_TOKENS , "Exceeds maximum token supply.");
        require(numTokens > 0 && numTokens <= 100, "Machine can dispense a minimum of 1, maximum of 100 tokens");

        for (uint i = 0; i < numTokens; i++) {
            _tokenIds.increment();
            _mint(msg.sender, _tokenIds.current());
            _setTokenURI(_tokenIds.current(), Strings.toString(_tokenIds.current()));
        }
    }
}